import java.io.*;
import java.net.*;

class UDPClient
{
   public static void main(String args[]) throws Exception
   {
	DatagramSocket clientSocket = new DatagramSocket();
	InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
	
	
	while(true){
	 System.out.print("Input: ");
	 BufferedReader con = new BufferedReader(new InputStreamReader(System.in));
	String sentence=con.readLine();
      byte[] sendData = new byte[1024];
	sendData = sentence.getBytes();
      	DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 2020);
      	clientSocket.send(sendPacket);
      byte[] receiveData = new byte[1024];

      DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
      clientSocket.receive(receivePacket);
      String modifiedSentence = new String(receivePacket.getData());
      System.out.println("FROM SERVER:" + modifiedSentence);
      //clientSocket.close();
   }
  }
}
